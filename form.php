<!DOCTYPE html>
<html lang="ru">

<head>
    <title>Задание 3</title>
    <link rel="stylesheet" href=style.css>
    <meta charset="utf-8">
</head>

<body>
    <header>
        <img src="logo.png" id="logo" alt="logo">
        <div class="head">
            <h2 id="virhushka"> Аппетитная страничка</h2>
        </div>
    </header>

    <form action="" method="POST">
        <h2 id="forms">Форма</h2>
        <table>
            <tr>
                <td>

                    <label>
		  Nickname:<br>
		  <input name="field-name-1">
		</label><br>

                    <label>
		  Электронный почтовый ящик:<br/>
		  <input name="field-email" type="email">
		</label><br>

                    <label>
		  Дата рождения:<br>
		  <input name="field-date" type="date">
		</label><br>
                    <label>
			Суперспособности:<br>
			<select name="field-name-3">
			  <option value="Недееспособность">Недееспособность</option>
			  <option value="Платёжеспособность" selected="selected">Платёжеспособность</option>
			  <option value="Талантлив(а) во всём">Талантлив(а) во всём</option>
			</select>
		  </label><br>
                </td>
                <td> <label>
		  Кем вы видите себя через 10 лет?:<br>
		  <textarea rows="18" cols="25" name="field-name-2">начальное значение</textarea>
		</label><br>
                </td>
                <td>
                    <label>
		  Питса:
		  <br>
		  <select name="field-name-4[]" multiple="multiple">
			<option value="Пепперона">Пепперона</option>
			<option value="С Ветчиной и Грибами" selected="selected">С Ветчиной и Грибами</option>
			<option value="С ананасом и курицей-_-" selected="selected">С ананасом и курицей-_-</option>
		  </select>
		</label><br> Любишь питсу на сколько баллов?:<br>
                    <label><input type="radio" checked="checked" name="radio-group-2" value="10">10</label><br>
                    <label><input type="radio" name="radio-group-2" value="12">12</label><br>
                    <br> Пол:<br>
                    <label><input type="radio" checked="checked" name="radio-group-1" value="М">М</label><br>
                    <label><input type="radio" name="radio-group-1" value="Ж">Ж</label>
                    <br> Чекбокс:<br>
                    <label><input type="checkbox" checked="checked" name="check-1">
		При нажатии кнопки "КЛАЦ" обещаю поставить автомат Батюшину Игорю Владимировичу</label><br>
                    <br> <input type="submit">

            </tr>
        </table>
    </form>
    </div>
    <footer>@Basya(G_G) -
        <a href="#virhushka" style="font-size: 20pt;">top</a>
    </footer>
</body>

</html>